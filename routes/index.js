/**
 * @external express Libreria de servidor web
 */
 const express = require('express');
  
 /**
  * @constant app para el uso de servidor web
  * @type express
  */
 const app = express();
 
 
 /**
  * @requires jokes
  * @description En este archivo estarán todas las direcciones de los microservicios para jokes
  */
  app.use(require('./jokes/routes/index'));
 
 
 
 /**
  * @exports app configuración para express
  */
 module.exports = app; 
 
