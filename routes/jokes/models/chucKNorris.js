/**
 * @constant mongoose
 * @description Sirve para definer el nombre de la colecciòn en moongo
 */
const mongoose = require("mongoose");

/**
 * @constant uniqueValidator
 * @description Sirve para verificar que no se repitan los campos sean valores unicos
 */
const uniqueValidator = require("mongoose-unique-validator");



/**
 * @constant Schema
 * @description creamos el esquema de mongoo o la colección
 */
let Schema = mongoose.Schema;

let infChuckNorrisSchema = new Schema({
  categories: {
    type: [Array],
  },
  createdAt: {
    type: Date,
    required: true,
  },
  iconUrl: {
    type: String,
    required: true,
  },
  idCN: {
    type: String,
    required: true,
    unique : true
  },
  updatedAt: {
    type: Date,
    required: true,
  },
  url: {
    type: String,
  },
  value: {
    type: String,
  },
  registerSystems: {
    type: Date,
    default: new Date(),
  }
});


infChuckNorrisSchema.plugin(uniqueValidator, {
  message: "{PATH} debe de ser único",
});

module.exports = mongoose.model("infChuckNorris", infChuckNorrisSchema);
