/**
 * @constant mongoose
 * @description Sirve para definer el nombre de la colecciòn en moongo
 */
 const mongoose = require("mongoose");

 /**
  * @constant uniqueValidator
  * @description Sirve para verificar que no se repitan los campos sean valores unicos
  */
 const uniqueValidator = require("mongoose-unique-validator");
 
 
 
 /**
  * @constant Schema
  * @description creamos el esquema de mongoo o la colección
  */
 let Schema = mongoose.Schema;
 
 let searchChuckNorrisSchema = new Schema({
   palabra: {
     type: String,
     required : true,
   },
   infChuckNorris : {
       type: Schema.ObjectId,
       ref : 'infChuckNorris',
       required : true,
   },
   consultaDesde:{
     type: Date,
     default : new Date()
   },
   ultimaConsulta:{
     type: Date,
     default : new Date()
   }
   
 });
 
 
 searchChuckNorrisSchema.plugin(uniqueValidator, {
   message: "{PATH} debe de ser único",
 });
 
 module.exports = mongoose.model("searchChuckNorris", searchChuckNorrisSchema);
 