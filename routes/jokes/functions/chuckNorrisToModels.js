const ChuckNorris = require("./../models/chucKNorris");
const SearchChuckNorris = require("./../models/search");

module.exports.guardarInformacionFromChuckNorris = async function (json, res) {
  let categories = json.categories;
  let createdAt = json.created_at;
  let iconUrl = json.icon_url;
  let idCN = json.id;
  let updatedAt = json.updated_at;
  let url = json.url;
  let value = json.value;
  let registerSystems = json.registerSystems;
  let chucKNorris = new ChuckNorris({
    categories,
    createdAt,
    iconUrl,
    idCN,
    updatedAt,
    url,
    value,
    registerSystems,
  });

  ChuckNorris.findOne({ idCN }).exec(async (err, regEnc) => {
    if (!regEnc) {
      await chucKNorris.save((error, result) => {
        if (error || !result) {
          return res.status(422).json({
            ok: false,
            message:
              "Existe un error en el registro de la cita, posiblemente no aparezca en el reporte",
            chuckNorris: result,
            error,
          });
        } else {
          return res.status(201).json({
            ok: true,
            message: "Se guardo correctamente la cita",
            chuckNorris: result,
          });
        }
      });
    } else {
      return res.status(400).json({
        ok: false,
        message:
          "Ya existe el siguiente registro, no lo duplicaremos en el reporte",
        chuckNorris: regEnc,
      });
    }
  });
};

module.exports.guardarInformacionBusquedaFromChuckNorris = async function (
  registros,
  res
) {
  
  for (let i = 0; i < registros.length; i++) {
    
    let lstRegistros = registros[i].total?registros[i].chuckNorris:[];

    if (lstRegistros.length) {
      for (let j = 0; j < lstRegistros.length; j++) {
        let inf = lstRegistros[j];
        let categories = inf.categories;
        let createdAt = inf.created_at;
        let iconUrl = inf.icon_url;
        let idCN = inf.id;
        let updatedAt = inf.updated_at;
        let url = inf.url;
        let value = inf.value;
        let registerSystems = inf.registerSystems;
        let chucKNorris = new ChuckNorris({
          categories,
          createdAt,
          iconUrl,
          idCN,
          updatedAt,
          url,
          value,
          registerSystems,
        });
        await ChuckNorris.findOne({ idCN }).exec(async (err, reg) => {
          let search = new SearchChuckNorris({
            palabra: registros[i].palabra,
            infChuckNorris: undefined,
          });

          if (reg) {
            SearchChuckNorris.findOne({infChuckNorris:reg._id})
            .exec(async (error, regRealizado)=>{
                if (!regRealizado){
                    search.infChuckNorris = reg._id;
                    await search.save();
                } else {
                    regRealizado.ultimaConsulta= new Date();
                    regRealizado.save();
                }
            });
            
          } else {
            await chucKNorris.save(async (err, info) => {
              if (info) {
                search.infChuckNorris = info._id;
                await search.save();
              }
            });
          }
        });
      }
    }
  }
};

module.exports.ObtenerTodosRegistrosDBApp = function(res){
  ChuckNorris.find().exec((err, registros)=>{
    if (err){
      return res.status(422).json({
        ok:false,
        message : 'No hay registros'
      });
    } else {
      return res.status(422).json({
        ok:true,
        message : 'Listado correctamente',
        numRegistros : registros.length,
        registros
      });
    }
  });
}

module.exports.ObtenerTodasLasConsultas = function(res){
  SearchChuckNorris.find()
  .populate('infChuckNorris')
  .exec((err, registros)=>{
    if (err){
      return res.status(422).json({
        ok:false,
        message : 'No hay registros'
      });
    } else {
      return res.status(422).json({
        ok:true,
        message : 'Listado correctamente',
        numRegistros : registros.length,
        registros
      });
    }
  });
}
