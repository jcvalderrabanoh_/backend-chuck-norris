const axios = require("axios");

const direccionSistema = process.env.apiChuckNorris;

/**
 * @name obtenerInformacionAleatoria
 * @description Devuelve información aleatorio de la api de chuck nurris
 */
module.exports.obtenerInformacionAleatoria = async function () {
  let direccion = direccionSistema + "jokes/random/";
  const chuckNorris = axios.create({
    baseURL: direccion,
    headers: {},
  });
  try {
    const informacion = await chuckNorris
      .get()
      .then((resp) => {
        return resp.data;
      })
      .catch((err) => {
        return {};
      });
    return informacion;
  } catch (error) {
    return {};
  }
};

/**
 * @name obtenerInformacionByCategoria
 * @description Devuelve información por categoria aleatoria de la api de chuck nurris
 */
module.exports.obtenerInformacionByCategoria = async function (categoria) {
  let direccion = direccionSistema + "jokes/random?category=" + categoria;
  const chuckNorris = axios.create({
    baseURL: direccion,
    headers: {},
  });
  try {
    const informacion = await chuckNorris
      .get()
      .then((resp) => {
        return resp.data;
      })
      .catch((err) => {
        return {};
      });
    return informacion;
  } catch (error) {
    return {};
  }
};

/**
 * @name obtenerInformacionByCategoria
 * @description Devuelve información aleatorio de la api de chuck nurris
 */
module.exports.isCategoriaValida = async function (categoria) {
  let direccion = direccionSistema + "jokes/categories";
  const chuckNorris = axios.create({
    baseURL: direccion,
    headers: {},
  });
  try {
    const informacion = await chuckNorris
      .get()
      .then((resp) => {
        let categorias = resp.data;
        let band = false;

        categorias.forEach((element) => {
          if (categoria == element) {
            band = true;
          }
        });
        return band;
      })
      .catch((err) => {
        console.log(err);
        return false;
      });
    return informacion;
  } catch (error) {
    console.log(error);
    return false;
  }
};

/**
 * @name obtenerInformacionFiltro
 * @description Devuelve información aplicando el filtro de palabras de la api de chuck nurris
 */
module.exports.obtenerInformacionFiltro = async function (cadenaInicial) {
  let separador = " ";
  let palabras = cadenaInicial.split(separador);
  palabras.push(cadenaInicial);
  if (!palabras) {
    return false;
  } else if (palabras.length == 0) {
    return false;
  } else {
    let respuestaFiltro = [];
    for (let i = 0; i < palabras.length; i++) {
      let palabra = palabras[i];
      let direccion = direccionSistema + "jokes/search?query=" + palabra;
      const chuckNorris = axios.create({
        baseURL: direccion,
        headers: {},
      });
      try {
        const informacion = await chuckNorris
          .get()
          .then((resp) => {
            return resp.data;
          })
          .catch((err) => {
            return {};
          });
        respuestaFiltro.push({
          palabra,
          total: informacion.total ? informacion.total : 0,
          chuckNorris: informacion.result ? informacion.result : [],
        });
      } catch (error) {
        respuestaFiltro.push({ palabra: "", total: 0, chuckNorris: [] });
      }
    }

    return respuestaFiltro;
  }
};

module.exports.convertirBusquedaJson = function (json) {
  let resp = "";
  let title = "Resultado de su busqueda en nuestra API";
  let delimitador = "\n";
  for (let i = 0; i < json.length; i++) {
    let head = "Busqueda de palabra : " + json[i].palabra + "";
    let info = "Sin valor";

    if (json[i].chuckNorris.length > 0) {
      info = "";
      for (let j = 0; j < json[i].chuckNorris.length; j++) {
        delete json[i].chuckNorris[j].created_at;
        delete json[i].chuckNorris[j].updated_at;
        delete json[i].chuckNorris[j].id;
        info +=
          "categorias: " +
          json[i].chuckNorris[j].categories +
          delimitador +
          "Icono: " +
          json[i].chuckNorris[j].icon_url +
          delimitador +
          "Url: " +
          json[i].chuckNorris[j].url +
          delimitador +
          "Valor: " +
          json[i].chuckNorris[j].value +
          delimitador +
          delimitador;
      }
    }

    resp += head + delimitador + info + delimitador;
  }
  resp = title + delimitador + resp;
  return resp;
};
