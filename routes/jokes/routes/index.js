/**
 * @external express Libreria de servidor web
 */
 const express = require('express');
  
 /**
  * @constant app para el uso de servidor web
  * @type express
  */
 const app = express();
 
 
 /**
  * @requires joke
  * @description En este archivo estarán todas las direcciones de los microservicios para joke
  */
  app.use(require('./joke'));
 
 
 
 /**
  * @exports app configuración para express
  */
 module.exports = app; 
 
