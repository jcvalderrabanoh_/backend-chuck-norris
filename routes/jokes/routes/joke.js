/**
 * @external express Libreria de servidor web
 */
const express = require("express");

/**
 * @constant app para el uso de servidor web
 * @type express
 */
const app = express();

/**
 * Funcion de consulta de la api de Chuck Norris
 */
const {
  obtenerInformacionAleatoria,
  isCategoriaValida,
  obtenerInformacionByCategoria,
  obtenerInformacionFiltro,
  convertirBusquedaJson,
} = require("./../functions/chuckNorris");

const {
  guardarInformacionFromChuckNorris,
  guardarInformacionBusquedaFromChuckNorris,
  ObtenerTodosRegistrosDBApp,
  ObtenerTodasLasConsultas
} = require("./../functions/chuckNorrisToModels");

const { enviarResultados } = require("./../../../global/functions/smtp");



/**
 * @swagger
 * /randomly:
 *   tags: "Random"
 *   get:
 *     summary: devuelve un frase de chuck norris o una busqueda por palabras
 *     description: Regresa una frase aleatoria si no cuenta con parametros, pero si se utiliza el parametro filter devuelve un listado y combinado con el parametro email que es el correo del solicitante lo envia
*/
app.get("/randomly", async (req, res) => {
  let data = req.query;
  if (!data.filter) {
    let resp = await obtenerInformacionAleatoria();
    await guardarInformacionFromChuckNorris(resp, res);
  } else {
    let resp = await obtenerInformacionFiltro(data.filter);
    await guardarInformacionBusquedaFromChuckNorris(resp, res);
    
    if (data.email){
      let info = convertirBusquedaJson(resp);
      await enviarResultados(data.email, "Información de consulta", info);
    }
    
    return res.status(200).json({
      resp,
    });
  }
});

app.get("/categoria/:Categoria", async (req, res) => {
  let data = req.params;
  let categoria = data.Categoria;
  let resp = await isCategoriaValida(categoria);
  if (resp == true) {
    resp = await obtenerInformacionByCategoria(categoria);
    await guardarInformacionFromChuckNorris(resp, res);
  } else {
    return res.status(404).json({
      ok: false,
      message: "Categoria no valida",
    });
  }
});


app.get("/obtenerTodaslasFrasesConsultadas", async (req, res)=>{
  await ObtenerTodosRegistrosDBApp(res);
});


app.get("/obtenerTodaslasConsultas", async (req, res)=>{
  await ObtenerTodasLasConsultas(res);
});

/**
 * @exports app configuración para express
 */
module.exports = app;
