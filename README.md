# Chuck Norris Back
Desarrollo de app de consumo de backend de la api  [Chuck Norris](https://api.chucknorris.io/).
## Prueba de Desarrollo Rabbit

### Configuracion
La base de datos es configura en el archivo `bin/config` actualmente a punta a mongoatlas

El correo es configura en el archivo `bin/config` actualmente utiliza una cuenta de google

### ejecución
El servidor actualmente se puede correr con el comando `node start` o `node bin/www`, para correr los script de pruebas se utilizar `npm test`

### EndPoints
#### randomly
Devuelve una frase random [http://localhost:3000/randomly](http://localhost:3000/randomly)

Si se agrega parametros de filtro en la url o de consulta cambia su comportamiento y devuelve todos los valores de coincidencia

Con la siguiente url devuelve todos los registros que contengan anim 
[localhost:3000/randomly?filter=anim](localhost:3000/randomly?filter=anim)

Con la siguiente url devuelve todos los registros que contengan anim y ademas envia los parametros al correo 
[localhost:3000/randomly?filter=anim&email=jcvalderrabanoh@gmail.com](localhost:3000/randomly?filter=anim&email=jcvalderrabanoh@gmail.com)

#### categoria
Devuelve si existe la categoria, se reemplaza :Categoria por el valor solicitado [http://localhost:3000/categoria/:Categoria](http://localhost:3000/categoria/:Categoria)

#### obtenerTodaslasFrasesConsultadas
Devuelve todos los registros que han sido consultados o buscados en la api [http://localhost:3000/obtenerTodaslasFrasesConsultadas](http://localhost:3000/obtenerTodaslasFrasesConsultadas)

#### obtenerTodaslasConsultas
Devuelve todos los registros que han sido consultados o buscados en la api [http://localhost:3000/obtenerTodaslasConsultas](http://localhost:3000/obtenerTodaslasConsultas)
