/**
 * @constant mongoose
 * @implements mongoose
 * @description Sirve para definer el nombre de la colecciòn en moongo
 */
const mongoose = require('mongoose');


console.log("Iniciando proceso de conexión a base de datos");
/* ========================================================== 
   ================== Conexión a Mongo DB =================== 
   ========================================================== */
/**
 * @method mongoose.connect 
 * @description Conexión a base de datos de mongo
 */

if (process.env.Development=='MongoAtlas'){
    mongoose.connect(process.env.HostBD,{
      useNewUrlParser: true,
      useUnifiedTopology: true 
    },
    (err, res) => {
      if (err){
        console.error('Conexión a base de datos con error');
        console.error(err);
      }
      if (res) {
        console.log('Conexión a base de datos realizada correctamente');
      }
    
    }
  );
}else{  
  mongoose.connect(process.env.HostBD + process.env.DBPORT + '/' + process.env.DBNAME, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    },
    (err, res) => {
      if (err){
        console.error('Conexión a base de datos con error');
        console.error(err);
      }
      if (res) {
        console.log('Conexión a base de datos realizada correctamente');
      }

    }
  );
}

mongoose.set('useCreateIndex', true);
