
 /**
  * @name apiChuckNorris
  * @alias apiChuckNorris
  * @type environment
  * @constant apiChuckNorris
  * @description Dirección web de la api de Chuck Norris
  */
process.env.apiChuckNorris = process.env.apiChuckNorris || 'https://api.chucknorris.io/';



//---------------------------------------------------------------------------------------------//
//---------------------------------- Configuración de SMTP ------------------------------------//
//---------------------------------------------------------------------------------------------//

/**
 * @name SMTPSERVER
 * @alias SMTPSERVER
 * @type environment
 * @constant SMTPSERVER
 * @description Servidor de SMTP 
 */
 process.env.SMTPSERVER = process.env.SMTPSERVER || "smtp.gmail.com";

 /**
 * @name SMTPSSL
 * @alias SMTPSSL
 * @type environment
 * @constant SMTPSSL
 * @description Requiere SSL 
 */
 process.env.SMTPSSL = process.env.SMTPSSL || true;

 /**
 * @name SMTPTLS
 * @alias SMTPTLS
 * @type environment
 * @constant SMTPTLS
 * @description Requiere TLS
 */
 process.env.SMTPTLS = process.env.SMTPTLS || true;

 /**
 * @name SMTPAUTH
 * @alias SMTPAUTH
 * @type environment
 * @constant SMTPAUTH
 * @description Requiere autenticación
 */
 process.env.SMTPAUTH = process.env.SMTPAUTH || true;

 /**
 * @name SMTPPORTSSL
 * @alias SMTPPORTSSL
 * @type environment
 * @constant SMTPPORTSSL
 * @description Puerto SSL
 */
 process.env.SMTPPORTSSL = process.env.SMTPPORTSSL || 465;

 /**
 * @name SMTPPORTSTARTTLS
 * @alias SMTPPORTSTARTTLS
 * @type environment
 * @constant SMTPPORTSTARTTLS
 * @description Puerto STARTTLS
 */
 process.env.SMTPPORTSTARTTLS = process.env.SMTPPORTSTARTTLS || 587;

 /**
 * @name SMTPUSER
 * @alias SMTPUSER
 * @type environment
 * @constant SMTPUSER
 * @description Usuario SMTP
 */
 process.env.SMTPUSER = process.env.SMTPUSER || "jcvalderrabanoh.pruebas@gmail.com";

 /**
 * @name SMTPPASS
 * @alias SMTPPASS
 * @type environment
 * @constant SMTPPASS
 * @description Password de usuario smtp
 */
 process.env.SMTPPASS = process.env.SMTPPASS || "QAZ123sad";


 //---------------------------------------------------------------------------------------------//
 //---------------------------------- Configuración de base de datos ---------------------------//
 //---------------------------------------------------------------------------------------------//
 /**
  * @name DBPORT
  * @alias DBPORT
  * @type environment
  * @constant DBPORT
  * @description Puerto del servidor de base de datos Mongo
  */
  process.env.DBPORT = process.env.DBPORT || 27017;

 /**
  * @name DBNAME
  * @alias DBNAME
  * @type environment
  * @constant DBNAME
  * @description Nombre de la base de datos en Mongo
  */
 process.env.DBNAME = process.env.DBNAME || 'dbChuckNorris';
 
 /**
  * @name DBUSER
  * @alias DBUSER
  * @type environment
  * @constant DBUSER
  * @description Usuario de la base de datos en Mongo
  */
 process.env.DBUSER = process.env.DBUSER || 'juan';
 
 
 /**
  * @name DBPASSWORD
  * @alias DBPASSWORD
  * @type environment
  * @constant DBPASSWORD
  * @description Password de la base de datos en Mongo
  */
 process.env.DBPASSWORD = process.env.DBPASSWORD || 'pruebate';
 
 /**
  * @name HostBD
  * @alias HostBD
  * @type environment
  * @constant HostBD
  * @description Host de la base de datos en Mongo
  */
 process.env.Development = process.env.Development || "MongoAtlas" ;
 
 if (process.env.Development=='development'){
     process.env.HostBD = process.env.HostBD || 'mongodb://localhost:';
 }else if (process.env.Development=='MongoAtlas'){
     process.env.HostBD = process.env.HostBD || 'mongodb+srv://'+process.env.DBUSER+':'+process.env.DBPASSWORD+'@cluster0.n6wzd.mongodb.net/'+process.env.DBNAME+'?retryWrites=true&w=majority';
 }else{
     process.env.HostBD = process.env.HostBD || 'mongodb://localhost:';
 }
 