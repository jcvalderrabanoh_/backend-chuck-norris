const { ExpectationFailed } = require('http-errors');
const request = require('supertest');
require('./../bin/config');
const app = require('../app');



describe('App Chuck Norris', () => {
  it('Prueba Sitio No disponible', (done) => {
    request(app)
      .get('/')
      .expect(404)
      .end(function (err, res) {
          done();
      });
  });

  it('Prueba randomly petición normal', (done) => {
    request(app)
      .get('/randomly')
      .expect(201)
      .end(function (err, res) {
          done();
      });
  });

  it('Prueba randomly verificación de respuesta', (done) => {
    request(app)
      .get('/randomly')
      .expect(201)
      .end(function (err, res) {
        if (res.body.ok==true && res.body.message && res.body.chuckNorris ){
          done();
        } 
      });
  });

  it('Prueba randomly verificación de respuesta con filtro ', (done) => {
    request(app)
      .get('/randomly?filter=animo')
      .expect(200)
      .end(function (err, res) {
        if (res ){
          done();
        } 
      });
  }).timeout(10000);

  it('Prueba randomly verificación de respuesta con email', (done) => {
    request(app)
      .get('/randomly?filter=anim&email=jcvalderrabanoh@gmail.com')
      .expect(200)
      .end(function (err, res) {
        if (res){
          done();
        } 
      });
  }).timeout(30000);

  it('Verificar Categoria no valida', (done) => {
    request(app)
      .get('/categoria/da')
      .expect(400)
      .end(function (err, res) {
        if (res.body['ok']==false){
          done();
        } 
      });
  }).timeout(10000);

  it('Verificar listado de todas las frases consultadas a la API', (done) => {
    request(app)
      .get('/obtenerTodaslasFrasesConsultadas')
      .expect(200)
      .end(function (err, res) {
        if (res.body['ok']==true){
          done();
        } 
      });
  }).timeout(10000);

  it('Verificar listado de todas las frases filtradas a la API', (done) => {
    request(app)
      .get('/obtenerTodaslasConsultas')
      .expect(200)
      .end(function (err, res) {
        
          done();
        
      });
  }).timeout(10000);


  it('Verificar Categoria valida', (done) => {
    request(app)
      .get('/categoria/dev')
      .expect(200)
      .end(function (err, res) {
        
        if (res.body['chuckNorris']){
          done();
        } 
      });
  }).timeout(10000);

});
