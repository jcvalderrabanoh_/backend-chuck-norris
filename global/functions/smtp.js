const nodemailer = require('nodemailer');
let transport = nodemailer.createTransport({
    host: process.env.SMTPSERVER,
    port: process.env.SMTPPORTSSL,
    auth: {
       user: process.env.SMTPUSER,
       pass: process.env.SMTPPASS
    }
});

module.exports.enviarResultados =  function(destino, asunto, informacion){
    const message = {
        from: process.env.SMTPUSER, 
        to: destino,         
        subject: asunto, 
        text: informacion
    };
    transport.sendMail(message, function(err, info) {
        if (err) {
          return false;
        } else {
          return true;
        }
    });
}